                 COMP30019 Graphics and Interaction
                         Semester 2, 2013
                             Project 2 

                              tilt380 

                               README
                              
    The team
        523590   David Andika (dandika)
        531621   Jonathan Adhitama Wongsodihardjo (jawo)
        384881   Michael Jonathan (jonm)
        385788   Winston Widjaya (wwidjaya)

        
1   What the application does

    The application allows the user to play a simple casual 3D game. Please 
    see proposal for a detailed description of the game. 

    It uses DirectX for 3D rendering.
    
    
2   How to use it

    Requirements
        Deploy tilt380 on a computer with Windows 8 or above, Visual Studio
        2012 Professional or above, and SharpDX 2.5.0 or above installed. An
        accelerometer is required to play in Tablet Mode. 

    Menu
        The menu page provides a starting point of the app. It lets you 
        start playing in either Desktop or Tablet mode. It shows highscores
        and provides navigation into the game guide. 

    Guide (How To Play)
        The game guide provides an insight to the objective of the game 
        and how to play it. To move between pages using a pointing device, hover 
        above the content to reveal left and right arrows for navigation. When 
        using touch input, simply swipe left or right to browse guide pages. 

    Game
        In the game, you are in control of the hamster, which is placed on 
        an unstable platform. The weight of the hamster and other falling 
        objects will cause the platform to tilt according to the position of 
        these objects on the platform. Use the hamster�s weight to continuously 
        stabilize the platform, without falling off it. The score increases with 
        time, and is affected by the amount of falling objects present on the 
        platform. The platform always starts at its ideal, flat position at the 
        beginning of each game. 

        A light is positioned above the platform, and its reflections are 
        visible on the hamster-ball, all falling objects and the platform 
        itself. The reflection on the platform is especially useful to show how 
        much the platform has tilted and in what direction, which aims to help 
        the player in deciding where to move the hamster in order to stabilize 
        the platform. 

        To play in Desktop Mode, control the hamster�s motion using the four 
        arrow keys. The camera follows the hamster to a certain extent, at least 
        until the hamster falls off. 

        To play in Tablet Mode, control the hamster�s motion by tilting the 
        device. A tablet computer is most recommended. The screen of the device 
        should face upwards at the beginning of the game which is set as the 
        flat position for the platform. This is to mimic the way the platform is 
        displayed in Tablet mode. 

    General
        The menu page, the game guide, and the in-game overlays are all 
        navigable through touch input as well as standard pointing devices. They 
        are created using standard Windows Store XAML layouts.
        
        
3   How we modelled objects and entities

    The hamster-ball, falling objects and wobbling platform are modelled and 
    coloured (with plain colours only; their gradients in the game are due 
    to lighting effects) using Blender, and then imported as models. 

    The surrounding �box� in the game environment is directly modelled in 
    the code using vertices to form triangles, and then textured using 
    images. 
    

4   How we handled graphics and camera motion 

    The game uses a single camera, which either follows the hamster to some 
    extent in Desktop Mode, or stays stationary above the platform in Tablet 
    Mode. In Desktop mode, it repositions itself by querying the hamster�s 
    position on every frame, calculating its new position, and finally 
    updating its view accordingly. 

    The attributes of this single camera is used when rendering each object
    in the game. A modelled object is rendered in a different way to the
    surrounding backgrounds, for example, but they all access properties 
    from this camera. 

    The Phong shading effect is used to simulate the lighting effect on the 
    hamster-ball, falling objects, and the platform, which is defined in a 
    single file for all of them. 


5   External Resources

    We used SharpDX, a managed DirectX API to manage the shading and 
    rendering of the game. We also used some sound resources, which are:
    
        boing.wav
            http://static1.grsites.com/archive/sounds/cartoon/cartoon006.wav
            by Anonymous
            
        adrenaline.mp3
            http://www.melodyloops.com/tracks/adrenaline/
            by Dimitriy Rodionov 


Happy tilting, 

    The tilt380 Team 

    www.facebook.com/tilt380

