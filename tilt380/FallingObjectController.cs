﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace tilt380
{
    public class FallingObjectController
    {
        //Constants
        private const float spawnDecreaseRate = 0.95f; //Of previous spawn countdown
        private const float spawnIncreaseRate = 100; //In milliseconds
        private const float minimumSpawnCountDown = 500; //In milliseconds

        private const float initialPosY = 5f;
        private const float initialVelY = 0.08f;
        private const float speed = 0.02f;
        
        private const double angleFactor = 0.8;

        // Attributes
        private Tilt380Game game;
        private List<FallingObject> fallingObjects;

        private float lastSpawnCountDown = 5000;
        private float spawnCountDown = 1000;

        private Random random = new Random();

        public FallingObjectController(Tilt380Game game)
        {
            this.game = game;
            this.fallingObjects = new List<FallingObject>();
        }

        public int numAliveObjects()
        {
            var num = 0;
            foreach (FallingObject obj in fallingObjects)
            {
                num += (obj.state == Hamster.HamsterState.PLAYING) ? 1 : 0;
            }
            return num;
        }

        public void Update(GameTime gametime)
        {
            updateSpawn(gametime);

            List<FallingObject> toDelete = new List<FallingObject>();
            foreach (FallingObject obj in fallingObjects)
            {
                obj.Update(gametime);

                // Marking dead FallingObjects to be deleted
                if (obj.isDead())
                {
                    toDelete.Add(obj);
                }
            }

            for (int i = 0; i < fallingObjects.Count - 1; i++)
            {
                for (int j = i + 1; j < fallingObjects.Count; j++)
                {
                    fallingObjects[i].collide(fallingObjects[j]);
                }
            }

            // Deleting FallingObjects
            foreach (FallingObject del in toDelete)
            {
                this.fallingObjects.Remove(del);
            }
        }

        public void Draw(GameTime gametime)
        {
            foreach (FallingObject obj in fallingObjects)
            {
                obj.Draw(gametime);
            }
        }

        public List<FallingObject> getFallingObjects()
        {
            return this.fallingObjects;
        }


        /** Creates new FallingObject when it is appropriate to do so. */
        private void updateSpawn(GameTime gametime)
        {
            this.spawnCountDown -= gametime.ElapsedGameTime.Milliseconds;

            if (this.spawnCountDown <= 0)
            {
                // Create new FallingObject
                this.fallingObjects.Add(this.spawnFallingObject(this.game.platform.radius, this.game.platform.boundary));

                // If game is over, slow down spawn rate
                if (game.hamster.gameover())
                {
                    this.spawnCountDown = this.lastSpawnCountDown + FallingObjectController.spawnIncreaseRate;
                }
                else
                {
                    this.spawnCountDown = this.lastSpawnCountDown * FallingObjectController.spawnDecreaseRate;
                    this.spawnCountDown = (spawnCountDown < FallingObjectController.minimumSpawnCountDown) ? minimumSpawnCountDown : spawnCountDown;
                }

                // Updating spawncountdown based on its previous starting value
                this.lastSpawnCountDown = this.spawnCountDown;
            }
        }

        /** Create a new FallingObject at random position and velocity. */
        private FallingObject spawnFallingObject(float radius, float boundary)
        {
            Vector3 randUnitVector = this.randomPointOnUnitCircle();

            Vector3 pos = this.game.platform.pos - (randUnitVector * (radius + boundary));
            Vector3 velocity = this.generateVelocity(randUnitVector, radius, boundary);

            pos.Y = FallingObjectController.initialPosY;

            return game.makeToDisposeContent(new FallingObject(this.game, velocity, pos));
        }

        /** Creates a random Vector on the unit circle */
        private Vector3 randomPointOnUnitCircle()
        {
            double zVal = this.positiveOrNegative() * this.random.NextDouble();
            double xVal = this.positiveOrNegative() * Math.Sqrt(1 - zVal * zVal);
            return new Vector3((float)xVal, 0, (float)zVal);
        }

        /** Generates -1 or 1 at random. */
        private int positiveOrNegative()
        {
            int generatedRandomNumber = this.random.Next(2);
            if (generatedRandomNumber == 0)
            {
                return -1;
            }
            return 1;
        }

        /** Use the given unit vector to generate a random velocity. */
        private Vector3 generateVelocity(Vector3 unitVector, float radius, float boundary)
        {
            // Tilt unitVector direction (source of randomness here)
            float angleSpan = (float) (angleFactor * Math.Asin(radius / (radius + boundary)));
            float angleRotation = this.random.NextFloat(-angleSpan / 2, angleSpan / 2);
            Vector3 velocity = Vector3.TransformCoordinate(unitVector, Matrix.RotationY(angleRotation));

            // Set speed
            velocity *= FallingObjectController.speed;
            velocity.Y = FallingObjectController.initialVelY;

            return velocity;
        }
    }
}
