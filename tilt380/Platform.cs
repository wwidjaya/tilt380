﻿using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;

namespace tilt380
{
    public class Platform : GameObject
    {
        private static float maxRotation = 0.75f;
        private static float thickness = 2f / 117;
        private static Vector3 initialPos = new Vector3(0, -thickness, 0);
        private static String modelName = "Platform2";
        private static String effectName = "Phong";
        private static float inertia = 100000;

        public readonly float radius = 20f;
        public readonly int boundary = 5;

        public Vector2 rotation = new Vector2(0, 0);

        private Vector2 omega = new Vector2(0, 0);

        public Platform(Tilt380Game game)
            : base(game, initialPos, modelName, effectName)
        {
            BasicEffect.EnableDefaultLighting(this.model, true);

            this.effect = game.Content.Load<Effect>("Phong");
        }

        public float getHeight(float X, float Z, float radius)
        {
            Vector3 point = new Vector3(X, radius, Z);

            point = Vector3.TransformCoordinate(point, Matrix.RotationX(rotation.Y));
            point = Vector3.TransformCoordinate(point, Matrix.RotationZ(-rotation.X));
            
            return point.Y;
        }

        public Vector3 getPos(Vector3 pos, float radius)
        {
            Vector3 point = new Vector3(pos.X, radius, pos.Z);

            point = Vector3.TransformCoordinate(point, Matrix.RotationX(rotation.Y));
            point = Vector3.TransformCoordinate(point, Matrix.RotationZ(-rotation.X));
            point = Vector3.TransformCoordinate(point, Matrix.Translation(0, pos.Y, 0));

            return point;
        }

        public Boolean isOnPlatform(Vector3 point)
        {
            var delta_x = this.pos.X - point.X;
            var delta_z = this.pos.Z - point.Z;

            var s2 = (delta_x * delta_x) + (delta_z * delta_z);
            var r2 = radius * radius;

            return (s2 <= r2) ? true : false;
        }

        public Boolean isInBound(Vector3 point)
        {
            var delta_x = this.pos.X - point.X;
            var delta_z = this.pos.Z - point.Z;

            var s2 = (delta_x * delta_x) + (delta_z * delta_z);
            var r2 = (radius + boundary) * (radius + boundary);

            return (s2 <= r2) ? true : false;
        }

        public override void Update(GameTime gametime)
        {
            base.Update(gametime);

            // Time elapsed since last update in milliseconds
            var delta = gametime.ElapsedGameTime.Milliseconds;

            // Calculate rotation based on objects on platform
            calculateRotation(delta);

            // And then update rotational velocity
            // (less lag in this order)
            calculateOmega();
            
            // Rotate to accomodate PLY
            this.world = Matrix.RotationX((float)Math.PI / 2);

            // Rotate platform wrt hamster
            this.world *= Matrix.Translation(initialPos);
            this.world *= Matrix.Scaling(radius);
            
            this.world *= Matrix.RotationX(rotation.Y);
            this.world *= Matrix.RotationZ(-rotation.X);
        }

        private void calculateRotation(int delta)
        {
            this.rotation.X += omega.X * delta / inertia;
            this.rotation.Y += omega.Y * delta / inertia;

            if (rotation.X > maxRotation)
            {
                rotation.X = maxRotation;
            }
            else if (rotation.X < -maxRotation)
            {
                rotation.X = -maxRotation;
            }

            if (rotation.Y > maxRotation)
            {
                rotation.Y = maxRotation;
            }
            else if (rotation.Y < -maxRotation)
            {
                rotation.Y = -maxRotation;
            }
        }

        private void calculateOmega()
        {
            this.omega = Vector2.Zero;
            calculateOmega(game.hamster);
            foreach (Hamster fallingObject in game.fallingObjectController.getFallingObjects())
            {
                calculateOmega(fallingObject);
            }
            return;
        }

        private void calculateOmega(Hamster hamster)
        {
            var alpha = (hamster.state == Hamster.HamsterState.PLAYING && isOnPlatform(hamster.pos)) ? new Vector2(hamster.pos.X, hamster.pos.Z) * hamster.radius : Vector2.Zero;

            // Blue buffs increases mass by 5
            this.omega += (hamster.isPlayer && hamster.type == 2) ? 5 * alpha : alpha;

            return;
        }
    }
}
