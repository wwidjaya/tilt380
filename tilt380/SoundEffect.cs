﻿using System;
using Windows.ApplicationModel;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace tilt380
{
    public class SoundEffect
    {
        private String filename;
        public SoundEffect(String filename) 
        {
            this.filename = filename;
        }

        public async void play() 
        {
            MediaElement snd = new MediaElement();
            StorageFolder folder = await Package.Current.InstalledLocation.GetFolderAsync("Sounds");
            StorageFile file = await folder.GetFileAsync(this.filename);
            var stream = await file.OpenAsync(FileAccessMode.Read);
            snd.SetSource(stream, file.ContentType);
            snd.Play();
        }
    }
}
