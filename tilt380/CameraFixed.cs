﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tilt380
{
    class CameraFixed : Camera
    {
        public CameraFixed(Tilt380Game game) : base(game)
        {
            // Setting default values for Camera View parameters
            cameraPos = new Vector3(0, 50, 0);
            cameraTarget = new Vector3(0, 0, 0);
            cameraUp = Vector3.UnitZ;

            // Setting default values for Camera Projection parameters
            fieldOfView = (float)Math.PI / 3.5f;
            znear = 0.1f;
            zfar = 10000f;

            // Initialize
            refreshAll();
        }
    }
}
