﻿using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using SharpDX.Toolkit.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit.Input;

namespace tilt380
{
    public class Hamster : GameObject
    {
        public static Vector3 initialPos = new Vector3(0, 0, 0);
        public static Vector3 initialVel = new Vector3(0, 0.15f, 0);
        public static float startingYPos = 5f;
        public static float finalDeathYPos = -200f;
        
        public readonly Boolean isPlayer;

        private static float damping = 0.75f; // How elastic the collisions are. 1 = almost perfectly
        private static float gravity = -0.000325f; // units/ms^2, Affects trajectory
        private static float agility = 0.9f; // Affects keyboard input
        private static float inertia = 10000; // Affects manual acceleration of Hamster on plane
        private static float friction = 25000; // Affects automatic acceleration of Hamster
        private static String[] modelNames = { "RegR", "RegG", "RegB", "GreySphere" };
        private static String effectName = "Phong";
        private static String stunSoundName = "boing.wav";

        private static Model[] models = new Model[modelNames.Length];

        private static int stunDuration = 250;
        private static int buffDuration = 4000;

        public float radius = 1;
        public int type = modelNames.Length - 1;

        protected Vector3 velocity = initialVel;

        private int stunRemaining = 0;
        private int buffRemaining = 0;
        private static SoundEffect stunnedSound = new SoundEffect(stunSoundName);

        public enum HamsterState
        {
            STARTING,
            PLAYING,
            DYING,
            DEAD
        }

        public HamsterState state;

        public Hamster(Tilt380Game game, Vector3 velocity, Vector3 pos)
            : base(game, pos, modelNames[modelNames.Length -1], effectName)
        {
            this.velocity = velocity;
            this.isPlayer = false;

            this.state = HamsterState.STARTING;
        }

        public Hamster(Tilt380Game game)
            : base(game, initialHighPos, modelNames[modelNames.Length - 1], effectName)
        {
            this.isPlayer = true;

            this.state = HamsterState.STARTING;

            for (int i = 0; i < modelNames.Length; i++)
            {
                models[i] = game.Content.Load<Model>(modelNames[i]);
            }
        }

        public override void Update(GameTime gametime)
        {
            base.Update(gametime);

            var delta = gametime.ElapsedGameTime.Milliseconds;
            switch (this.state)
            {
                case HamsterState.PLAYING:
                    accelerateManual(delta);
                    accelerateAuto(delta);
                    move(delta);
                    updateBuff(delta);
                    updateState();
                    updateScore(delta); //Updates the score of the player
                    break;
                default:
                    accelerateFalling(delta);
                    move(delta);
                    updateState();
                    break;
            }

            // Apply pos to world
            this.world = Matrix.Translation(game.platform.getPos(this.pos, this.radius));
        }

        private void updateBuff(int delta)
        {
            if (!isPlayer)
            {
                return;
            }
            if (this.buffRemaining > 0)
            {
                this.buffRemaining -= delta;
                if (this.buffRemaining <= 0)
                {
                    this.type = modelNames.Length - 1;
                }
            }
            this.model = models[type];
        }

        private void updateState()
        {
            switch (state)
            {
                case HamsterState.STARTING:
                    if (this.pos.Y <= Hamster.initialPos.Y)
                    {
                        if (game.platform.isOnPlatform(this.pos))
                        {
                            state = HamsterState.PLAYING;
                            translateFallingVelocity();
                        }
                        else
                        {
                            state = HamsterState.DYING;
                        }
                    }
                    break;
                case HamsterState.PLAYING:
                    this.pos.Y = Hamster.initialPos.Y;
                    state = (game.platform.isOnPlatform(this.pos)) ? state : HamsterState.DYING;
                    break;
                case HamsterState.DYING:
                    state = (this.pos.Y > Hamster.finalDeathYPos) ? state : HamsterState.DEAD;
                    break;
                case HamsterState.DEAD:
                default:
                    break;
            }
            return;
        }

        protected Vector3 translateFallingVelocity()
        {
            this.velocity = damping * game.platform.getPos(new Vector3(velocity.X, 0, velocity.Z), -velocity.Y * damping);
            this.velocity.Y = (this.state == HamsterState.PLAYING) ? 0 : -this.velocity.Y;
            return this.velocity;
        }

        private void accelerateManual(int delta)
        {
            if (!this.isPlayer)
            {
                return;
            }
            if (this.stunRemaining > 0 || this.type == 0)
            {
                this.stunRemaining -= delta;
                return;
            }
            
            switch (game.inputManager.mode)
            {
                case InputManager.InputMode.DESKTOP:
                    KeyboardState keyboardState = game.inputManager.keyboardState;
                    if (keyboardState.IsKeyDown(Keys.Left))
                    {
                        this.velocity.X -= Hamster.agility * delta / inertia;
                    }
                    if (keyboardState.IsKeyDown(Keys.Right))
                    {
                        this.velocity.X += Hamster.agility * delta / inertia;
                    }
                    if (keyboardState.IsKeyDown(Keys.Up))
                    {
                        this.velocity.Z += Hamster.agility * delta / inertia;
                    }
                    if (keyboardState.IsKeyDown(Keys.Down))
                    {
                        this.velocity.Z -= Hamster.agility * delta / inertia;
                    }

                    goto default;

                case InputManager.InputMode.TABLET:
                    var reading = game.inputManager.accelerometerReading;

                    this.velocity.X += (float)reading.AccelerationX * delta / inertia;
                    this.velocity.Z += (float)reading.AccelerationY * delta / inertia;

                    goto default;

                default:
                    return;
            }
        }

        private void accelerateAuto(int delta)
        {
            this.velocity.Y = 0;

            if (this.type == 1)
            {
                // Green balls are magnetic
                return;
            }
            else if (!isPlayer && this.type == 0)
            {
                // Red balls home in on player
                Vector3 homing = game.hamster.pos - this.pos;
                homing.Normalize();

                this.velocity.X += homing.X * delta / inertia;
                this.velocity.Z += homing.Z * delta / inertia;

                return;
            }

            this.velocity.X += game.platform.rotation.X * delta / friction;
            this.velocity.Z += game.platform.rotation.Y * delta / friction;

            return;
        }

        private void accelerateFalling(int delta)
        {
            this.velocity.Y += gravity * delta;
            return;
        }

        private void move(int delta)
        {
            this.pos += this.velocity * delta;

            bool starting = state == HamsterState.STARTING;
            bool fellthru = this.pos.Y < initialPos.Y;
            bool notinstadead = game.platform.isOnPlatform(this.pos);
            this.pos.Y = (starting && fellthru && notinstadead) ? initialPos.Y : this.pos.Y;

            return;
        }

        public Vector3 collide(Vector3 impactDirection, Vector3 impactVelocity, float impactRadius, int impactType)
        {
            if (isPlayer)
            {
                buff(impactType);
            }

            this.stunRemaining = Hamster.stunDuration;
            Hamster.stunnedSound.play();

            // Bigger impact = bigger loss of energy
            Vector3 impact = impactDirection * (impactVelocity - this.velocity).LengthSquared();
            float massDifference = (float)Math.Sqrt(impactRadius / this.radius);

            // Blue buffs increases mass by 5
            massDifference /= (isPlayer && this.type == 2) ? 5 : 1;

            Vector3 action = damping * damping * massDifference * (impactVelocity - impact);
            Vector3 reaction = damping * damping / massDifference * (this.velocity + impact);

            this.velocity = action;
            this.velocity.Y = 0;
            return reaction;
        }

        public Boolean gameover()
        {
            return (state == HamsterState.DEAD);
        }

        private static Vector3 initialHighPos
        {
            get { return new Vector3(initialPos.X, initialPos.Y + startingYPos, initialPos.Z); }
        }

        private void buff(int type)
        {
            switch (type)
            {
                case 0:
                    // Red ones disable other buffs, and stun 3 times as hard
                    this.type = type;
                    this.buffRemaining = 3 * stunDuration;
                    break;
                case 1:
                case 2:
                    // Extend buff duration is same type
                    if (this.type == type)
                    {
                        this.buffRemaining = buffDuration;
                    }
                    // Buffs don't overwrite each other
                    if (buffRemaining <= 0)
                    {
                        this.type = type;
                        this.buffRemaining = buffDuration;
                    }
                    break;
                default:
                    break;
            }
        }

        private void updateScore(int delta)
        {
            if (isPlayer)
            {
                var mod = (game.inputManager.mode == InputManager.InputMode.DESKTOP) ? 0.75f : 1;
                var alive = 1 + game.fallingObjectController.numAliveObjects();
                game.score += (uint)(mod * delta * alive * alive);
            }
            return;
        }
    }
}
