﻿using System;
using System.Collections.Generic;
using Windows.Storage;

namespace tilt380
{
    public static class HighscoreStorage
    {
        public static List<uint> highscores;
        public const int maxSize = 10;

        public static void loadHighscores()
        {
            try
            {
                HighscoreStorage.convertToList(Windows.Storage.ApplicationData.Current.LocalSettings.Values["highscores"].ToString());
            }
            catch (Exception e)
            {
                HighscoreStorage.highscores = new List<uint>();
            }
        }

        public static void updateHighscore(uint score)
        {
            if (HighscoreStorage.highscores.Contains(score))
            {
                return;
            }

            if (HighscoreStorage.highscores.Count >= HighscoreStorage.maxSize)
            {
                if (score > HighscoreStorage.highscores[0])
                {
                    HighscoreStorage.highscores[0] = score;
                }
                HighscoreStorage.highscores.Sort();
            }
            else
            {
                HighscoreStorage.highscores.Add(score);
            }
        }

        public static void resetHighscore()
        {
            HighscoreStorage.highscores.Clear();
        }

        public static void saveHighscores()
        {
            try
            {
                String toSave = convertToString();
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["highscores"] = toSave;
            }
            catch (Exception e)
            {
                throw new Exception("Failed to save Highscore!", e);
            }
        }

        private static string convertToString()
        {
            string output = "";
            foreach (uint i in HighscoreStorage.highscores) 
            {
                output += i.ToString() + ",";
            }
            if (output.Length > 0)
            {
                return output.Substring(0, output.Length - 1); //Ignores the last comma added
            }
            else
            {
                return output;
            }
        }

        private static void convertToList(string serial)
        {
            string[] scores = serial.Split(',');

            // Resetting the highscores
            if (HighscoreStorage.highscores == null)
            {
                HighscoreStorage.highscores = new List<uint>();
            }
            else
            {
                HighscoreStorage.highscores.Clear();
            }

            // Loading highscores from the string
            foreach (string i in scores) 
            {
                HighscoreStorage.highscores.Add(Convert.ToUInt32(i));
            }
            HighscoreStorage.highscores.Sort();
        }
    }
}
