﻿using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tilt380
{
    public class FallingObject : Hamster
    {
        private static String[] modelNames = { "SmallR", "SmallG", "SmallB", "BigBlack" };
        private static float[] radii = { 0.75f, 0.75f, 0.75f, 1.5f };

        public FallingObject(Tilt380Game game, Vector3 velocity, Vector3 pos)
            : base (game, velocity, pos)
        {
            randomizeThrownObjectModel();
        }

        public override void Update(GameTime gametime)
        {
            base.Update(gametime);

            this.collide(this.game.hamster);
        }

        public bool isDead()
        {
            return gameover();
        }

        /** Checks for collision. */
        public void collide(Hamster hamster)
        {
            Vector3 v = hamster.pos - this.pos;

            var hitRadius = hamster.radius + this.radius;
            
            if (v.LengthSquared() < hitRadius*hitRadius)
            {
                this.velocity = (this.state == HamsterState.STARTING) ? translateFallingVelocity() : this.velocity;
                this.velocity = hamster.collide(v, this.velocity, this.radius, this.type);

                hamster.pos += v / 10;
                this.pos -= v / 10;

                if (hamster.isPlayer && type == 0)
                {
                    this.type = (hamster.type != 0 && hamster.type != modelNames.Length - 1) ? hamster.type : new Random().Next(modelNames.Length - 1);
                    this.model = game.Content.Load<Model>(modelNames[type]);
                }
            }
        }

        /** Denotes the FallingObject as dead. */
        private void die()
        {
            this.state = HamsterState.DEAD;
        }

        private void randomizeThrownObjectModel()
        {
            if (modelNames.Length != radii.Length)
            {
                throw new Exception("Falling object selection issue.");
            }

            var random = new Random();
            int selected = random.Next((modelNames.Length - 1) * 3);
            selected = (selected < modelNames.Length - 1) ? selected : modelNames.Length - 1;

            this.model = game.Content.Load<Model>(modelNames[selected]);
            this.radius = radii[selected];
            this.type = selected;

            // Red ones go faster.
            this.velocity *= (selected == 0) ? 1.25f : 1;
        }
    }
}
