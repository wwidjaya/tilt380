﻿using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Sensors;
using Windows.UI.Xaml;
using Windows.UI.Input;
using Windows.UI.Core;


namespace tilt380
{
    public class InputManager : IDisposable
    {
        public enum InputMode
        {
            DESKTOP, TABLET
        }

        public InputMode mode;

        private Tilt380Game game;

        // Desktop mode
        private KeyboardManager keyboardManager;
        public KeyboardState keyboardState;
        
        // Tablet mode
        private CoreWindow window;
        private GestureRecognizer gestureRecognizer;
        private Accelerometer accelerometer;
        public AccelerometerReading accelerometerReading;
        
        public InputManager(Tilt380Game game, InputMode mode)
        {
            this.accelerometer = Accelerometer.GetDefault();

            this.game = game;
            this.mode = (accelerometer == null) ? SetupDesktop() : Setup(mode);

            return;
        }

        private InputMode Setup(InputMode mode)
        {
            switch (mode)
            {
                case InputMode.DESKTOP:
                    SetupDesktop();
                    goto default;
                case InputMode.TABLET:
                    SetupTablet();
                    goto default;
                default:
                    return mode;
            }
        }

        private InputMode SetupDesktop()
        {
            this.keyboardManager = new KeyboardManager(game);

            return InputMode.DESKTOP;
        }

        private InputMode SetupTablet()
        {
            SetupDesktop();

            // Get the accelerometer object
            window = Window.Current.CoreWindow;

            // Set up the gesture recognizer.  In this example, it only responds to TranslateX and Tap events
            gestureRecognizer = new Windows.UI.Input.GestureRecognizer();
            gestureRecognizer.GestureSettings = GestureSettings.ManipulationTranslateX | GestureSettings.ManipulationScale | GestureSettings.Tap;

            // Register event handlers for pointer events
            window.PointerPressed += OnPointerPressed;
            window.PointerMoved += OnPointerMoved;
            window.PointerReleased += OnPointerReleased;

            gestureRecognizer.Tapped += game.Tapped;
            gestureRecognizer.ManipulationStarted += game.OnManipulationStarted;
            gestureRecognizer.ManipulationUpdated += game.OnManipulationUpdated;
            gestureRecognizer.ManipulationCompleted += game.OnManipulationCompleted;
            
            return InputMode.TABLET;
        }

        // Update
        public void Update(GameTime gameTime)
        {
            switch(mode)
            {
                case InputMode.TABLET:
                    accelerometerReading = accelerometer.GetCurrentReading();
                    goto case InputMode.DESKTOP;
                case InputMode.DESKTOP:
                    keyboardState = keyboardManager.GetState();
                    return;
                default:
                    return;
            }
         }

        // Call the gesture recognizer when a pointer event occurs
        void OnPointerPressed(CoreWindow sender, PointerEventArgs args)
        {
            gestureRecognizer.ProcessDownEvent(args.CurrentPoint);
        }

        void OnPointerMoved(CoreWindow sender, PointerEventArgs args)
        {
            gestureRecognizer.ProcessMoveEvents(args.GetIntermediatePoints());
        }

        void OnPointerReleased(CoreWindow sender, PointerEventArgs args)
        {
            gestureRecognizer.ProcessUpEvent(args.CurrentPoint);
        }

        public void Dispose()
        {
            if (keyboardManager != null) {keyboardManager.Dispose();}
        }
    }
}