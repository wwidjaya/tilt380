﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace tilt380
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage
    {
        private Tilt380Game game;
        private tilt380.InputManager.InputMode mode;

        public GamePage(tilt380.InputManager.InputMode mode)
        {
            this.InitializeComponent();
            this.mode = mode;
            newGame();
        }

        private void ClickExit(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.game.Exit();
            Window.Current.Content = new Frame();
            (Window.Current.Content as Frame).Navigate(typeof(MenuPage));
        }

        private void ClickPause(object sender, RoutedEventArgs e)
        {
            if (this.game.togglePause())
            {
                showPause();
            }
            else
            {
                hidePause();
            }
        }

        private void ClickPlayAgain(object sender, RoutedEventArgs e)
        {
            this.game.Exit();
            Window.Current.Content = new GamePage(this.mode);
        }

        private void newGame()
        {
            hideGameover();
            hidePause();
            game = new Tilt380Game(this, mode);
            game.Run(this);
        }

        public void hideGameover()
        {
            this.RectangleGameOver.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.TextBlockGameOverScoreHeading.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.TextBlockGameOverScoreValue.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.ButtonPlayAgain.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            this.TextBlockScoreHeading.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.TextBlockScoreDisplay.Visibility = Windows.UI.Xaml.Visibility.Visible;

            this.ButtonPause.IsEnabled = true;
        }

        public void showGameover(uint highscore)
        {
            this.TextBlockGameOverScoreValue.Text = highscore.ToString();

            this.RectangleGameOver.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.TextBlockGameOverScoreHeading.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.TextBlockGameOverScoreValue.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.ButtonPlayAgain.Visibility = Windows.UI.Xaml.Visibility.Visible;

            this.TextBlockScoreHeading.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.TextBlockScoreDisplay.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            this.ButtonPause.IsEnabled = false;
        }

        public void hidePause()
        {
            this.TextBlockPausedHeading.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.TextBlockPausedDesc.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.RectanglePause.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.ButtonPause.Content = "PAUSE";
        }

        public void showPause()
        {
            this.TextBlockPausedHeading.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.TextBlockPausedDesc.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.RectanglePause.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.ButtonPause.Content = "RESUME";
        }

        public void showScore(uint score)
        {
            this.TextBlockScoreDisplay.Text = score.ToString();
        }
    }
}
