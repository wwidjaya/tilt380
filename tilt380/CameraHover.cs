﻿using SharpDX;
using SharpDX.Toolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tilt380
{
    class CameraHover : Camera
    {
        public static float initialY = 30;
        public static float initialZ = -30;
        public static float finalY = 50;
        public static float ZtoYratio = 1.5f;
        public static float zoomSpeed = 0.0003f;
        public static float zoomOmega = 0.0002f;

        public Vector3 offset = new Vector3(0, initialY, initialZ);

        public float tracking = 0.2f;

        public CameraHover(Tilt380Game game)
            : base(game)
        {
            // Setting default values for Camera View parameters
            cameraPos = new Vector3(offset.X, offset.Y, offset.Z);
            cameraTarget = new Vector3(0,0,0);
            cameraUp = Vector3.UnitY;

            // Setting default values for Camera Projection parameters
            fieldOfView = (float)Math.PI / 4.0f;
            znear = 0.1f;
            zfar = 10000f;

            // Initialize
            refreshAll();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (!((game.hamster.state == Hamster.HamsterState.DYING) || (game.hamster.state == Hamster.HamsterState.DEAD)))
            {
                // Standard update
                Vector3 cPos = game.platform.getPos(game.hamster.pos + offset, game.hamster.radius) - cameraPos;
                Vector3 tPos = game.platform.getPos(game.hamster.pos, game.hamster.radius) - cameraTarget;

                cameraPos += new Vector3(
                    cPos.X * tracking,
                    cPos.Y,
                    cPos.Z * tracking);

                cameraTarget += tPos * tracking;
            }
            else
            {
                // Zoom out if dying
                int delta = gameTime.ElapsedGameTime.Milliseconds;

                float yPos = finalY - cameraPos.Y;
                Vector3 tPos = Vector3.Zero - cameraTarget;

                cameraPos.Y += yPos * zoomSpeed * delta;
                cameraPos.Z -= yPos * ZtoYratio * zoomSpeed * delta;
                cameraTarget += tPos * zoomSpeed * delta;

                cameraPos = Vector3.TransformCoordinate(cameraPos, Matrix.RotationY(zoomOmega * delta));
            }

            refreshAll();
        }
    }
}
