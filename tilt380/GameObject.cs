﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace tilt380
{
    using SharpDX.Toolkit.Graphics;
    abstract public class GameObject : IDisposable
    {
        public Tilt380Game game;
        public Vector3 pos;
        public Effect effect;
        public Model model;
        public Matrix world;

        public GameObject(Tilt380Game game, Vector3 initialPos, String modelName, String effectName)
        {
            this.game = game;
            this.pos = initialPos;
            this.model = game.Content.Load<Model>(modelName);
            this.effect = game.makeToDisposeContent(game.Content.Load<Effect>(effectName));
            this.world = Matrix.Identity;
        }

        public virtual void Update(GameTime gametime)
        {
        }

        public virtual void Draw(GameTime gametime)
        {
            Matrix worldInverseTranspose = Matrix.Transpose(Matrix.Invert(this.world));
            this.effect.Parameters["World"].SetValue(this.world);
            this.effect.Parameters["Projection"].SetValue(game.camera.Projection);
            this.effect.Parameters["View"].SetValue(game.camera.View);
            this.effect.Parameters["cameraPos"].SetValue(game.camera.cameraPos);
            this.effect.Parameters["worldInvTrp"].SetValue(worldInverseTranspose);

            model.Draw(game.GraphicsDevice, this.world, game.camera.View, game.camera.Projection, this.effect);
        }

        public void Dispose()
        {
            this.effect.Dispose();
            this.model.Dispose();
        }
    }
}
