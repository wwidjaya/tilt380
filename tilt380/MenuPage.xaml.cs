﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Devices.Sensors;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace tilt380
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MenuPage : Page
    {
        public MenuPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.refreshHighscores();
        }

        private void ClickPlayTablet(object sender, RoutedEventArgs e)
        {
            if (Accelerometer.GetDefault() != null)
            {
                Window.Current.Content = new GamePage(tilt380.InputManager.InputMode.TABLET);
            }
            else
            {
                var msg = new Windows.UI.Popups.MessageDialog("You need an accelerometer to play tilt380 in tablet mode.");
                msg.ShowAsync();
            }
        }

        private void ClickPlayPC(object sender, RoutedEventArgs e)
        {
            Window.Current.Content = new GamePage(tilt380.InputManager.InputMode.DESKTOP);
        }

        private void refreshHighscores()
        {
            HighscoreStorage.loadHighscores();
            List<uint> highscores = HighscoreStorage.highscores;

            // Refreshing highscore list
            this.clearHighscores();
            for (int i = highscores.Count - 1; i >= 0; i--)
            {
                uint highscore = highscores.ElementAt(i);
                ListViewItem highscoreEntry = CreateHighScoreListEntry(highscore);
                highscoreList.Items.Add(highscoreEntry);
            }
        }

        private void clearHighscores()
        {
            List<ListViewItem> toDelete = new List<ListViewItem>();

            foreach (ListViewItem entry in highscoreList.Items)
            {
                if (entry.Equals(sampleHighscoreEntry))
                {
                    continue;
                }
                toDelete.Add(entry);
            }

            foreach (ListViewItem del in toDelete)
            {
                highscoreList.Items.Remove(del);
            }

            sampleHighscoreEntry.Visibility = Visibility.Visible;
        }

        private ListViewItem CreateHighScoreListEntry(uint highscore)
        {
            sampleHighscoreEntry.Visibility = Visibility.Collapsed;

            ListViewItem entry = new ListViewItem();

            // Styling the entry
            entry.Padding = sampleHighscoreEntry.Padding;
            entry.VerticalContentAlignment = sampleHighscoreEntry.VerticalContentAlignment;
            entry.FontSize = sampleHighscoreEntry.FontSize;
            entry.Background = sampleHighscoreEntry.Background;
            entry.Foreground = sampleHighscoreEntry.Foreground;

            entry.Content = highscore;

            return entry;
        }

        private void ClickGuide(object sender, RoutedEventArgs e)
        {
            Window.Current.Content = new Frame();
            (Window.Current.Content as Frame).Navigate(typeof(HelpPage));
        }

        private void btnResetHighscore_Click(object sender, RoutedEventArgs e)
        {
            HighscoreStorage.resetHighscore();
            HighscoreStorage.saveHighscores();
            this.refreshHighscores();
        }
    }
}
