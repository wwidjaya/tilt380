﻿using SharpDX;
using SharpDX.Toolkit.Graphics;
using System;

namespace tilt380
{
    public class Background : IDisposable
    {
        public static float scale = 75f;

        // Game reference
        public Tilt380Game game;

        // Model
        public SharpDX.Toolkit.Graphics.Buffer vertices;
        public VertexInputLayout inputLayout;
        public int vertexStride;
        public Texture2D texture;

        // Properties
        public Matrix world;
        public BasicEffect effect;

        public Background(Tilt380Game game, Matrix world, String imagePath)
        {
            // Game reference
            this.game = game;

            // Model
            this.vertices = SharpDX.Toolkit.Graphics.Buffer.Vertex.New(game.GraphicsDevice, createShapeArray());
            this.inputLayout = VertexInputLayout.New<VertexPositionTexture>(0);
            this.vertexStride = Utilities.SizeOf<VertexPositionTexture>();
            this.texture = game.Content.Load<Texture2D>(imagePath);//  ("SeaPMC.jpg");

            // Properties
            //this.world = Matrix.Identity;
            this.world = world; // Matrix.RotationY((float)Math.PI / 2) * Matrix.Translation(50, 0, 0);
            this.effect = new BasicEffect(game.GraphicsDevice)
                {
                    View = game.camera.View,
                    Projection = game.camera.Projection,
                    World = this.world,
                    Texture = this.texture,
                    TextureEnabled = true,
                    VertexColorEnabled = false
                };
        }

        public void Draw()
        {
            // Effect updates
            this.effect.View = game.camera.View;
            this.effect.Projection = game.camera.Projection;
            this.effect.World = this.world;

            // Setup to draw
            game.GraphicsDevice.SetVertexBuffer(0, this.vertices, this.vertexStride);
            game.GraphicsDevice.SetVertexInputLayout(this.inputLayout);

            // Apply and draw
            this.effect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, this.vertices.ElementCount);
        }

        public VertexPositionTexture[] createShapeArray()
        {
            VertexPositionTexture[] shapeArray = new VertexPositionTexture[]{
                new VertexPositionTexture(new Vector3(-scale, scale, 0f), new Vector2(0f, 0f)), // Top left
                new VertexPositionTexture(new Vector3(scale, scale, 0f), new Vector2(1f, 0f)), // Top right
                new VertexPositionTexture(new Vector3(scale, -scale, 0f), new Vector2(1f, 1f)), // Bottom right
                
                new VertexPositionTexture(new Vector3(-scale, -scale, 0f), new Vector2(0f, 1f)), // Bottom left
                new VertexPositionTexture(new Vector3(-scale, scale, 0f), new Vector2(0f, 0f)), // Top left
                new VertexPositionTexture(new Vector3(scale, -scale, 0f), new Vector2(1f, 1f)), // Bottom right
            };
            return shapeArray;
        }

        public void Dispose()
        {
            this.vertices.Dispose();
            this.texture.Dispose();
            this.effect.Dispose();
        }
    }
}
