﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace tilt380
{
    public abstract class Camera
    {
        public Tilt380Game game;

        // Camera properties
        public Matrix View;
        public Matrix Projection;

        // Camera View parameters
        public Vector3 cameraPos;
        public Vector3 cameraTarget;
        public Vector3 cameraUp;

        // Camera Projection parameters
        public float fieldOfView;
        public float znear;
        public float zfar;
        public float aspectRatio;

        // Ensures that all objects are being rendered from a consistent viewpoint
        public Camera(Tilt380Game game)
        {
            // Store reference to game
            this.game = game;
        }

        // Initialize view (must call this method after setting up View parameters)
        public void refreshView()
        {
            View = Matrix.LookAtLH(cameraPos, cameraTarget, cameraUp);
        }

        // Initialize view (must call this method after setting up Projection parameters)
        public void refreshProjection()
        {
            aspectRatio = (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height;
            Projection = Matrix.PerspectiveFovLH(fieldOfView, aspectRatio, znear, zfar);
        }

        // Refresh view and projection
        public void refreshAll()
        {
            refreshView();
            refreshProjection();
        }

        public virtual void Update(GameTime gameTime)
        {
        }
    }
}
