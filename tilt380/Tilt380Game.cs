﻿using SharpDX;
using SharpDX.Toolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Sensors;
using Windows.UI.Input;

namespace tilt380
{
    public class Tilt380Game : Game
    {
        // I/O
        public GraphicsDeviceManager graphicsDeviceManager;
        public InputManager inputManager;
        
        //Constants
        public int MAX_X = 800;
        public int MAX_Y = 600;
        public int MAX_Z = 600;
        public int minimumHeight = 700;
        public int minimumHeightToSpawn = 200; //The minimum height that falling objects can spawn (can't get lower than this!)
        
        // Elements
        public Camera camera;
        public uint score = 0;

        // GameObjects
        public Platform platform;
        public Hamster hamster;
        public FallingObjectController fallingObjectController;

        // Misc
        public GamePage mainPage;
        public bool paused;
        public bool updatedHighscore;

        // BG
        public List<Background> backgrounds;

        public Tilt380Game(GamePage mainPage, InputManager.InputMode mode)
        {
            this.graphicsDeviceManager = new GraphicsDeviceManager(this);
            this.inputManager = this.makeToDisposeContent(new InputManager(this, mode));

            Content.RootDirectory = "Content";

            this.mainPage = mainPage;
            this.paused = false;
            this.updatedHighscore = false;
            
            this.backgrounds = new List<Background>();
        }

        protected override void LoadContent()
        {
            if (inputManager.mode == InputManager.InputMode.DESKTOP)
            {
                this.camera = new CameraHover(this);
            }
            else
            {
                this.camera = new CameraFixed(this);
            }

            this.platform = this.makeToDisposeContent(new Platform(this));
            this.hamster = this.makeToDisposeContent(new Hamster(this));
            this.fallingObjectController = new FallingObjectController(this);

            HighscoreStorage.loadHighscores();

            addBackgrounds();

            base.LoadContent();
        }

        private void addBackgrounds()
        {
            String imagePath = "MJSBG.jpg"; // Might wanna use different backgrounds for each side
            // Rear
            this.backgrounds.Add(this.makeToDisposeContent(new Background(this, Matrix.Translation(0, 0, Background.scale), imagePath)));
            // Right
            this.backgrounds.Add(this.makeToDisposeContent(new Background(this, Matrix.RotationY((float)Math.PI / 2) * Matrix.Translation(Background.scale, 0, 0), imagePath)));
            // Left
            this.backgrounds.Add(this.makeToDisposeContent(new Background(this, Matrix.RotationY((float)Math.PI / 2 * -1) * Matrix.Translation(-Background.scale, 0, 0), imagePath)));
            // Bottom
            this.backgrounds.Add(this.makeToDisposeContent(new Background(this, Matrix.RotationX((float)Math.PI / 2) * Matrix.Translation(0, -Background.scale, 0), imagePath)));
            // Front
            this.backgrounds.Add(this.makeToDisposeContent(new Background(this, Matrix.RotationY((float)Math.PI) * Matrix.Translation(0, 0, -Background.scale), imagePath)));
        }

        protected override void Update(GameTime gameTime)
        {
            if (paused)
            {
                return;
            }

            inputManager.Update(gameTime);

            platform.Update(gameTime);

            hamster.Update(gameTime);

            mainPage.showScore(this.score);

            fallingObjectController.Update(gameTime);

            camera.Update(gameTime);

            base.Update(gameTime);

            if (gameover())
            {
                mainPage.showGameover(this.score);

                if (!this.updatedHighscore)
                {
                    HighscoreStorage.updateHighscore(this.score);
                    HighscoreStorage.saveHighscores();
                    this.updatedHighscore = true;
                }
            }
            else
            {
                if (this.updatedHighscore)
                {
                    this.updatedHighscore = false;
                }
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            // Drawing Platform
            this.platform.Draw(gameTime);

            // Drawing Hamster
            this.hamster.Draw(gameTime);

            // Drawing Falling Objects
            fallingObjectController.Draw(gameTime);

            // Drawing BG
            foreach (Background bg in backgrounds)
            {
                bg.Draw();
            }

            base.Draw(gameTime);
        }

        public Boolean gameover()
        {
            return hamster.gameover();
        }

        public void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {
            // Pass Manipulation events to the game objects.
            /*foreach (var obj in gameObjects)
            {
                obj.Tapped(sender, args);
            }*/
        }

        public void OnManipulationStarted(GestureRecognizer sender, ManipulationStartedEventArgs args)
        {
            // Pass Manipulation events to the game objects.

        }

        public void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
            /*
            //TASK 3: Update camera position when scaling occurs
            camera.pos.Z = camera.pos.Z * args.Delta.Scale;
            // Update camera position for all game objects
            foreach (var obj in gameObjects)
            {
                if (obj.basicEffect != null) { obj.basicEffect.View = camera.View; }

                // TASK 4: Respond to OnManipulationUpdated events for linear motion
                obj.OnManipulationUpdated(sender, args);
            }*/
        }

        public void OnManipulationCompleted(GestureRecognizer sender, ManipulationCompletedEventArgs args)
        {
        }

        public bool togglePause()
        {
            this.paused = !this.paused;
            return this.paused;
        }

        public T makeToDisposeContent<T>(T disposable) where T : IDisposable
        {
            return ToDisposeContent<T>(disposable);
        }

        protected override void EndRun()
        {
            this.UnloadContent();
            base.EndRun();
        }
    }
}
